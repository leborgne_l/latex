# Modèle LaTeX

Un modèle simple pour compiler des documents LaTeX et les publier
directement dans LaForgeEdu

Le fichier Makefile ne s'occupera que de compiler les derniers documents produits. Ils sont ensuite commis automatiquement dans le dépôt.

Pour que ceux-ci soient ensuite ajouter au dépôt dans une version correspondant à ce qui vient d'être commis, il faut [créer un jeton `CI_PUSH_TOKEN`](-/settings/access_tokens) qu'on doit [ajouter dans les variables](-/settings/ci_cd).